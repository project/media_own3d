<?php

/**
 * @file
 * Displays a Own3d video.
 *
 * Available variables:
 * - $width: The width of the video.
 * - $height: The height of the video.
 * - $movie: The embed URL of the video.
 * - $allowscriptaccess: A parameter on the video object.
 * - $allowfullscreen: A parameter on the video object.
 * - $wmode: A parameter on the video object.
 *
 * @see media_own3d_preprocess_media_own3d_video()
 *
 * @ingroup themable
 */

?>
<div class="media-own3d-outer-wrapper" id="media-own3d-<?php print $id; ?>" style="width: <?php print $width; ?>px; height: <?php print $height; ?>px;">
  <div class="media-own3d-wrapper" id="<?php print $wrapper_id; ?>" >
    <object width="<?php print $width; ?>" height="<?php print $height; ?>">
      <param name="movie" value="<?php print $movie; ?>" />
      <param name="allowscriptaccess" value="<?php print $allowscriptaccess; ?>" />
      <param name="allowfullscreen" value="<?php print $allowfullscreen; ?>" />
      <param name="wmode" value="<?php print $wmode; ?>" />
      <embed src="<?php print $movie; ?>" type="application/x-shockwave-flash" allowfullscreen="<?php print $allowfullscreen; ?>" allowscriptaccess="<?php print $allowscriptaccess; ?>" width="<?php print $width; ?>" height="<?php print $height; ?>" wmode="<?php print $wmode; ?>"></embed>
    </object>
  </div>
</div>
