Media: Own3d
------------

Provides Own3d support for the Media module. Videos and thumbnails can be
displayed as rendered files after importing using the video page URL.

Maintainers
-----------

 * Niklas Fiekas
   http://drupal.org/user/1089248
   niklas.fiekas@tu-clausthal.de
