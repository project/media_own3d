<?php

/**
 * @file
 * Own3d Api class.
 */
class MediaOwn3dApi {

  public function __construct() {
    $this->liveCheckUrl = 'http://api.own3d.tv/liveCheck.php';
    $this->apiUrl = 'http://api.own3d.tv/api.php';
  }

  public function validLiveId() {
    if (isset($this->liveId) && is_numeric($this->liveId)) {
      $this->getLiveStream();
      if (is_numeric($this->liveStream['liveViewers'])) {
        return TRUE;
      }
    }
    return FALSE;
  }

  public function validStaticId() {
    if (isset($this->singleVideoId) && is_numeric($this->singleVideoId)) {
      $this->getSingleVideo();
      if (!empty($this->singleVideo['misc']['game_short'])) {
        return TRUE;
      }
    }
    return FALSE;
  }

  public function getSingleVideo() {
    $this->filterParameters = "single_video_id=" . $this->singleVideoId;
    $file_url = $this->apiUrl . "?" . $this->filterParameters;
    $xml = simplexml_load_file($file_url, 'SimpleXMLElement', LIBXML_NOCDATA);
    $video = array();
    foreach ($xml->channel->item as $item) {
      foreach (get_object_vars($item) as $key => $val) {
        if ($key == "misc") {
          $misc = get_object_vars($val->attributes());
          $val = $misc['@attributes'];
        }
        $video[$key] = $val;
      }
      $this->singleVideo = $video;
    }
  }

  public function getLiveStream() {
    $this->filterParameters = "live_id=" . $this->liveId;
    $file_url = $this->liveCheckUrl . "?" . $this->filterParameters;
    $xml = simplexml_load_file($file_url, 'SimpleXMLElement', LIBXML_NOCDATA);
    $live = array();
    foreach ($xml->liveEvent as $item) {
      foreach (get_object_vars($item) as $key => $val) {
        if ($key == "misc") {
          $misc = get_object_vars($val->attributes());
          $val = $misc['@attributes'];
        }
        $live[$key] = $val;
      }
      $this->liveStream = $live;
    }
  }

}
